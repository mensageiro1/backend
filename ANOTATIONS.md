# Anotações

O projeto foi desenvolvido em TypeScript.

## Configurações iniciais

### Iniciando o projeto

    yarn init -y
    yarn add express
    yarn add @types/express -D
    yarn add typescript -D

    yarn tsc --init

Após criação do arquivo `tsconfig.json`, deve-se colocar o campo strict como `false`. Para utilizar os decorators, deve-se adicionar/descomentar os campos **emitDecorationMetadata** e **experimentalDecorators** com o valor `true`.

```json
	"strict": false,
	// ....

```

Tradutor pro `node`:

    yarn add ts-node-dev -D

Adiciona o script no package.json:

```json
	"scripts": {
		"dev": "ts-node-dev src/server.ts"
	}
```

Para rodar o servidor:

    yarn dev

### Instalação das bibliotecas para acesso ao banco de dados

O banco de dados utilizado será o SQLite.

    yarn add typeorm reflect-metadata sqlite3

Criar o arquivo `ormconfig.json` na raiz do projeto e adicionar as seguintes configurações:

```json
{
  "type": "sqlite"
}
```

Criar um arquivo `index.ts` dentro de `src/database` e digitar o seguinte código:

```typescript
import { createConnection } from "typeorm";

createConnection();
```

Importar o banco de dados no arquivo `server.ts`

```typescript
import "./database"; // cria a conexao com o banco de dados
```

### Criação de migrations

Para criar as migrations, deve-se criar uma pasta `migrations` dentro da pasta `database`. Modificar o arquivo `ormconfig`:

```json
{
  "type": "sqlite",
  "database": "./src/database/database.sqlite",
  "migrations": ["./src/database/migrations/**.ts"],
  "entities": ["./src/entities/**.ts"],
  "cli": {
    "migrationsDir": "./src/database/migrations"
  }
}
```

Criar um script no `package.json` para criar e rodar nossas migrations

```json
	...
	"scripts": {
	    "dev": "ts-node-dev src/server.ts",
	    "typeorm": "ts-node-dev node_modules/typeorm/cli.js"
	  },
	...
```

Para criar uma migration utilizar o comando
  
    yarn typeorm migration:create -n <NomeMigration>

Exemplo:
yarn typeorm migration:create -n CreateSettings

Dentro do arquivo da migration você encontrará duas funções assincronas: `up` e `down`. A função `up` é utilizada para rodar a migration e a função `down` é utilizada para remover a tabela criada.

```typescript
await queryRunner.createTable(
  new Table({
    name: "settings",
    columns: [
      {
        name: "id",
        type: "uuid",
        isPrimary: true,
      },
      {
        name: "username",
        type: "varchar",
      },
      {
        name: "chat",
        type: "boolean",
        default: true,
      },
      {
        name: "created_at",
        type: "timestamp",
        default: "now()",
      },
    ],
  })
);
```

Para executar a migration

    yarn typeorm migration:run

Para remover todas as tabelas criadas pelas migrations:

    yarn typeorm schema:drop

### Criando entidades e repositorios

Criar uma pasta `entities` dentro de `src`. Dentro desta pasta serão criados os arquivos que representam as entidades do nosso banco de dados.

Para utilizar o tipo uuid iremos instalar a biblioteca `uuid`, junto com sua tipagem.

    	yarn add uuid
    	yarn add @types/uuid -D

Os repositórios ficarão na pasta `repositories` dentro de `src`.

Após esta configuração inicial, iremos criar os controllers.