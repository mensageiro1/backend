let socket_admin_id = null;
let emailUser = null;
let socket = null;

document.querySelector("#start_chat").addEventListener("click", function (e) {
  socket = io();

  // hide the button
  document.querySelector("#start_chat").style.display = "none";
  // hide input email
  document.querySelector("#email").style.display = "none";

  // show new button
  document.querySelector("#new_message").style.display = "block";

  const text = document.querySelector("#txt_help").value;
  const email = document.querySelector("#email").value;
  emailUser = email;

  socket.on("connect", () => {
    const params = { email, text };

    socket.emit("client_first_access", params, (call, err) => {
      if (err) {
        console.log(err);
      } else {
        console.log(call);
      }
    });
  });

  socket.on("client_list_all_messages", (messages) => {
    // console.log("mensagens: ", messages);

    var template_client = document.querySelector(
      "#message-user-template"
    ).innerHTML;
    var template_admin = document.querySelector("#admin-template").innerHTML;

    messages.forEach((message) => {
      if (message.admin_id === null) {
        const rendered = Mustache.render(template_client, {
          message: message.text,
          email,
        });

        document.getElementById("messages").innerHTML += rendered;
      } else {
        const rendered = Mustache.render(template_admin, {
          message_admin: message.text,
        });
        document.getElementById("messages").innerHTML += rendered;
      }
    });
  });

  socket.on("admin_send_to_client", (message) => {
    socket_admin_id = message.socket_id;
    const template_admin = document.querySelector("#admin-template").innerHTML;

    const rendered = Mustache.render(template_admin, {
      message_admin: message.text,
    });

    document.getElementById("messages").innerHTML += rendered;
  });
});

document.querySelector("#new_message").addEventListener("click", function (e) {
  const text = document.querySelector("#txt_help");

  const params = { text: text.value, socket_admin_id };
  socket.emit("client_send_to_admin", params);

  const template_client = document.querySelector(
    "#message-user-template"
  ).innerHTML;

  const rendered = Mustache.render(template_client, {
    message: text.value,
    email: emailUser,
  });

  document.getElementById("messages").innerHTML += rendered;
});
